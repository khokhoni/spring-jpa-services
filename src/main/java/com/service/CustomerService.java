package com.service;

import com.bean.Customer;
import com.repository.CustomerRepository;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService implements CustomerInterface {

    @Autowired
    private CustomerRepository repository;

    @Override
    public List<Customer> findAll() {

        //List<Customer> customers = (List<Customer>) repository.findAll();
    	
    	  ArrayList<Customer> customers = new ArrayList<Customer>();
    	  Customer cust1= new Customer(Long.valueOf("1"),"Khokhoni");
    	  Customer cust2= new Customer(Long.valueOf("2"),"Khetho");
    	  Customer cust3= new Customer(Long.valueOf("3"),"Edgar");
    	  Customer cust4= new Customer(Long.valueOf("4"),"Mkhuseli");
    	  Customer cust5= new Customer(Long.valueOf("5"),"Syndy");
    	  Customer cust6= new Customer(Long.valueOf("6"),"Monde");
    	  customers.add(cust1);
    	  customers.add(cust2);
    	  customers.add(cust3);
    	  customers.add(cust4);
    	  customers.add(cust5);
    	  customers.add(cust6);
        return customers;
    }
}