package com.service;

import com.bean.Customer;
import java.util.List;

public interface CustomerInterface{

    public List<Customer> findAll();
}