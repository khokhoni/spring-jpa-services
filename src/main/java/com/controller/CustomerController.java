package com.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;
import java.util.Map;


import com.service.CustomerInterface;
import com.repository.CustomerRepository;
import com.bean.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//testing
@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private CustomerInterface customerInterface;
      
    @RequestMapping("/customers")
    //@GetMapping("/customers")
    public List<Customer> getAllCustomers()  {
        return customerRepository.findAll();
    } 

    @GetMapping("/customers/{id}")
    public Customer retrieveCustomer(@PathVariable long id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (!customer.isPresent()) System.out.println("Hi");
            //throw new CustomerNotFoundException("id-" + id);

        return customer.get();
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomer(@PathVariable long id) {
        customerRepository.deleteById(id);
    }


    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
        Customer savedCustomer = customerRepository.save(customer);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedCustomer.getId()).toUri();
        return ResponseEntity.created(location).build();

    }


    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@RequestBody Customer customer, @PathVariable long id) {

        Optional<Customer> customerOptional = customerRepository.findById(id);

        if (!customerOptional.isPresent())
            return ResponseEntity.notFound().build();

        customer.setId(id);

        customerRepository.save(customer);

        return ResponseEntity.noContent().build();
    }
    
    

    @RequestMapping("/showCustomers")
    public ModelAndView findCustomers() {

        List<Customer> customers = (List<Customer>) customerInterface.findAll();

        Map<String, Object> params = new HashMap<>();
        params.put("customers", customers);

        return new ModelAndView("showCustomers", params);
    }
}