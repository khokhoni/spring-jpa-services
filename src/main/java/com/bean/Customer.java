package com.bean;
//testing
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@XmlRootElement
//@Table(name = "customer")
public class Customer {

        //@Column(name="id")
        @Id
        //@GeneratedValue(strategy=GenerationType.AUTO)
        private  Long id;

       // @Column(name="name")
        private   String name;

        //Default constructor
        public Customer(){      	
        }
        
        public Customer(Long id,String name) {
                this.id = id;
                this.name = name;
        }


        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }


        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }
        
        @Override
        public String toString() {
            return String.format(
                    "customer[id=%d, name='%s']",
                    id, name);
        }
        
}